﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;

namespace AnalyzeReceipt
{
    class AnalyzeReceipt
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Analyze Receipt on file \"sample-receipts.json\"");

            int counter = 0;
            int totald = 0, totals = 0, length = 0;
            string line;
            string prefix = @"\d{1,2}\s{2,10}";
            string suffix = @".*?\\r\\n";
            string sandwichPattern = prefix + @"\d{1,2}.""" + suffix;
            string drinkPattern = prefix + @"(\d{1,2}oz.(Fountain Drink|Icee)|Juice Box|[a-zA-Z0-9]{1,50} BtlDrk|Bottled [a-zA-Z0-9]{1,50}|[a-zA-Z0-9]{1,50}.{1,10}Lemonade|([-A-Za-z0-9]{1,10}){0,1}\s{0,1}Coffee\s{0,1}([a-zA-Z0-9\s]{1,50}){0,1})" + suffix;
            string freshValueMealPattern = prefix + @".{0,10}Fresh Value Meal" + suffix;
            string simple6Mealpattern = prefix + @"(\[N\])*Simple 6 Meal" + suffix;
            Regex srgx = new Regex(sandwichPattern, RegexOptions.IgnoreCase);
            Regex drgx = new Regex(drinkPattern, RegexOptions.IgnoreCase);
            Regex fvmrgx = new Regex(freshValueMealPattern, RegexOptions.IgnoreCase);
            Regex s6mrgx = new Regex(simple6Mealpattern, RegexOptions.IgnoreCase);
            Regex oneSpacergx = new Regex("[ ]{2,}", RegexOptions.None);

            // Read the file and display it line by line.
            System.IO.StreamReader file = null;
            try
            {
                file = new System.IO.StreamReader("sample-receipts.json");
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.Message);
                System.Console.WriteLine("Please make sure to put file on the same directory as the executable");
                System.Console.WriteLine("Press any key to exit...");
                System.Console.ReadLine();
                Environment.Exit(-1);
            }

            while ((line = file.ReadLine()) != null)
            {
                //sandich order
                MatchCollection res = srgx.Matches(line);
                foreach (Match m in res)
                {
                    //Console.WriteLine(m.Value);
                    string temp = oneSpacergx.Replace(m.Value, " ");
                    string[] subStrings = temp.Split(' ');
                    int order = Int32.Parse(subStrings[0]);
                    int l = Int32.Parse(Regex.Match(subStrings[1], @"\d+").Value);
                    totals += order;
                    length += order * l;
                }

                //beverage order
                res = drgx.Matches(line);
                foreach (Match m in res)
                {
                    string[] subStrings = m.Value.Split(' ');
                    int order = Int32.Parse(subStrings[0]);
                    totald += order;
                }

                //fresh value meal order
                res = fvmrgx.Matches(line);
                foreach (Match m in res)
                {
                    string[] subStrings = m.Value.Split(' ');
                    int order = Int32.Parse(subStrings[0]);
                    totald += order;
                }

                //simple 6 meal order
                res = s6mrgx.Matches(line);
                foreach (Match m in res)
                {
                    string[] subStrings = m.Value.Split(' ');
                    int order = Int32.Parse(subStrings[0]);
                    totald += order;
                    totals += order;
                    length += order * 6;
                }

                if (++counter %1000 == 0)
                {
                    System.Console.Write("Parsing file: " + counter + "\r");
                }
            }

            file.Close();
            Console.WriteLine("Total Sandwich Sold: {0}\nTotal Sandwich Length: {1} foot\nTotal Drink Sold: {2}", totals, length/12.0, totald);
            System.Console.ReadLine();
        }
    }
}
