#!/bin/bash

if [ "$#" -eq  "0" ]
then
	echo "No filename supplied"
	exit 0
fi

# declare regex and filename
filename=$1
prefix='\d{1,2}\s{2,10}'
postfix='.*?\\r\\n'
sandwichRegex='(\d{1,2}.\")'
drinkRegex='(\d{1,2}oz.(Fountain Drink|Icee)|Juice Box|[a-zA-Z0-9]{1,50} BtlDrk|Bottled [a-zA-Z0-9]{1,50}|[a-zA-Z0-9]{1,50}.{1,10}Lemonade|([-A-Za-z0-9]{1,10}){0,1}\s{0,1}Coffee\s{0,1}([a-zA-Z0-9\s]{1,50}){0,1})'
freshvalueMealRegex='(.{0,10}Fresh Value Meal)'
simple6MealRegex='((\[N\])*Simple 6 Meal)'
allOrdersRegex=$prefix$postfix

# get all orders and store grep result
echo "Getting all orders"
all_res=$(grep -i -o -E "$allRegex" $filename)

# calculte result
echo "Caculating all orders of sandwich and drink"
# get result on sandwich(sandwich order)
echo "Caculating sandwich orders"
echo "Sandwich Example: 1   6\" Chicken Bcn Ranch Sub  4.75\r\n"
temp=$(echo "$all_res" | grep -i -o -E "$prefix$sandwichRegex$postfix" |
awk '
{
	TOTAL += $1
	LENGTH += $1 * $2
} END {
	print TOTAL+0" "LENGTH+0 
}')

set -- $temp
TOTALS=$1
LENGTHS=$2

# get result on drinks(drink order and fresh value meal)
echo "Caculating drink orders"
echo "Drink Example: 2      21oz Fountain Drink        3.50\r\n"
echo "Drink Example: 1      Milk BtlDrk   			   2.00\r\n"
echo "Drink Example: 1       -Free Coffee w/any Bkfst  0.00\r\n"
echo "Drink Example: 1      Bottled Carbonated Drink  $2.25\r\n"
echo "Drink Example: 1       -Fresh Value Meal (21-1)  2.60\r\n"
temp=$(echo "$all_res" | grep -i -o -E "$prefix($drinkRegex|$freshvalueMealRegex)$postfix" |
awk '
{
	TOTAL += $1
} END {
	print TOTAL+0
}')

set -- $temp
TOTALD=$1

# get result on simple 6 meal (sandwich and drink)
echo "Caculating simple 6 meal orders(sandwich and drink)"
echo "Example: 1      [N]Simple 6 Meal           6.00\r\n"
temp=$(echo "$all_res" | grep -i -o -E "$prefix($simple6MealRegex)$postfix" |
awk '
{
	TOTAL += $1
	LENGTH += $1 * 6
} END {
	print TOTAL+0" "LENGTH+0 
}')

set -- $temp
TOTALS=$((TOTALS + $1))
TOTALD=$((TOTALD + $1))
LENGTHS=$((LENGTHS + $2))

# print result
echo "Total Sandwich Sold: " $TOTALS 
echo "Total Length: "$((LENGTHS/12))" foot"
echo "Total Drinks Sold: " $TOTALD

